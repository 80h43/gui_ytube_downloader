from pytube import YouTube
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox

root = Tk()

root.title("YouTube Downloader")
root.geometry("400x300+430+180")

canvas = Canvas(root, width = 400, height= 200)
canvas.pack()

logo = PhotoImage(file = "ytube_logo.png")
logo = logo.subsample(2,2)
canvas.create_image(200 , 80, image = logo)

def download():
    yt_link = str(link.get())
    yt = YouTube(yt_link)
    stream = yt.streams.get_highest_resolution()
    stream.download()
    messagebox.showinfo("Information","Download Completed !!!")
    enter.delete(0, "end")


link = StringVar()

url = Label(root, text = "Enter Youtube URL", font= 5)
url.place(x = 130, y = 150)

enter = Entry(root, width = 40, textvariable = link)
enter.place(x = 80, y = 180)
  
dwn = Button(root, text = "Click to Download", command = download)
dwn.place(x = 150, y = 220)

root.mainloop()
